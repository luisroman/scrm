SCRM test

By Luis Roman

Used technology for the tests:
- Kotlin
- Spring Boot 2
- Appium
- JUnit 5
- Allure

How to execute the tests:
- You can execute the tests in 2 ways
    - Executing the tests from a IDE
    - Executing by command using: mvn test
- If you need to change the ip address where Appium is, you have to change it in the application.yml what you can find in src/main/resources
- After execute the tests you can generate a report executing: mvn allure:serve

How the framework works:
- The first what the framework does is generate an Android Driver to execute the tests, you can see in AppiumSetUp (right now is only with Android, but is possible to add more profiles for iOS, or to execute in a grid, or a farm, etc...)
- This driver is injected in the Pages (as here I used Page Object Pattern to be able to reuse and maintain easier) and Utils
- And these pages are injected into the tests
- Then the tests are executed
- Every time a test is executed is taking an screenshot at the end that is possible to check in the allure report (you can check the AppiumTestExecutionListener class to see how it works)
- After the tests are executed you can generate the report with: mvn allure:serve
