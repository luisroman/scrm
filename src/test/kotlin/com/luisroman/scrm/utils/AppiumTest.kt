package com.luisroman.scrm.utils

import com.luisroman.scrm.SpringConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestExecutionListeners

@SpringBootTest(classes = [SpringConfiguration::class])
@TestExecutionListeners(listeners = [AppiumTestExecutionListener::class])
annotation class AppiumTest