package com.luisroman.scrm.utils

import io.appium.java_client.android.AndroidDriver
import org.springframework.test.context.TestContext
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener

open class AppiumTestExecutionListener: DependencyInjectionTestExecutionListener() {

    override fun afterTestMethod(testContext: TestContext) {
        testContext.applicationContext.getBean(ContextUtils::class.java).changeToNative()
        testContext.applicationContext.getBean(ScreenshotUtils::class.java).takeScreenshot()
        testContext.applicationContext.getBean(AndroidDriver::class.java).resetApp()
        super.afterTestMethod(testContext)
    }

}