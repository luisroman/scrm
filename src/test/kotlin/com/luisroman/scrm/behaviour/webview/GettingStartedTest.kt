package com.luisroman.scrm.behaviour.webview

import com.luisroman.scrm.page.MenuBarPage
import com.luisroman.scrm.page.webview.GettingStartedWebPage
import com.luisroman.scrm.page.webview.MainWebPage
import com.luisroman.scrm.utils.AppiumTest
import com.luisroman.scrm.utils.ContextUtils
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@AppiumTest
@DisplayName("Getting Started Test")
open class GettingStartedTest {

    @Autowired
    lateinit var menuBarPage: MenuBarPage

    @Autowired
    lateinit var mainWebPage: MainWebPage

    @Autowired
    lateinit var gettingStartedWebPage: GettingStartedWebPage

    @Autowired
    lateinit var contextUtils: ContextUtils

    @Test
    fun `Open Getting Started Web`() {
        menuBarPage.clickWebViewButton()
        contextUtils.changeToWebView()
        mainWebPage.clickGetStartedButton()
        assertTrue(gettingStartedWebPage.checkIsGettingStartedCurrentUrl())
    }

}