package com.luisroman.scrm.behaviour.login

import com.luisroman.scrm.page.MenuBarPage
import com.luisroman.scrm.page.login.LoginPage
import com.luisroman.scrm.utils.AppiumTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@AppiumTest
@DisplayName("Login Test")
open class LoginTest {

    @Autowired
    lateinit var mainBarPage: MenuBarPage

    @Autowired
    lateinit var loginPage: LoginPage

    @Test
    fun `Do a Successful Login`() {
        mainBarPage.clickLoginButton()
        loginPage.login(EMAIL, PASSWORD)
        loginPage.clickSuccessLoginPopupConfirmButton()
        assertTrue(loginPage.isLoginButtonDisplayed())
    }

    companion object {
        private const val EMAIL = "luisrmn90@gmail.com"
        private const val PASSWORD = "Testing2019"
    }

}