package com.luisroman.scrm.behaviour.forms

import com.luisroman.scrm.page.MenuBarPage
import com.luisroman.scrm.page.forms.FormsPage
import com.luisroman.scrm.utils.AppiumTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@AppiumTest
@DisplayName("Forms Test")
open class FormsTest {

    @Autowired
    lateinit var menuBarPage: MenuBarPage

    @Autowired
    lateinit var formsPage: FormsPage

    @Test
    fun `Fill form in Forms screen`() {
        menuBarPage.clickFormsButton()
        formsPage.clickSwitchButton()
        assertTrue(formsPage.isSwitchChecked())
        formsPage.clickSwitchButton()
        assertFalse(formsPage.isSwitchChecked())
        formsPage.clickDropDownList()
        formsPage.clickAppiumIsAwesomeOption()
        assertTrue(formsPage.isAppiumIsAwesomeSelected())
        formsPage.clickActiveButton()
        formsPage.clickCancelPopupButton()
        assertTrue(formsPage.isActiveButtonDisplayed())
    }

}