package com.luisroman.scrm.config

import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.remote.AndroidMobileCapabilityType
import io.appium.java_client.remote.AutomationName
import io.appium.java_client.remote.MobileCapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.net.URL
import java.util.concurrent.TimeUnit
import javax.annotation.PreDestroy

@Configuration
open class AppiumSetUp {

    @Value("\${appium.url}")
    private lateinit var url: String

    private lateinit var appDriver: AndroidDriver<MobileElement>

    private val getAppPath = AppiumSetUp::class.java.getResource("/Android-NativeDemoApp-0.2.1.apk").path.replace("%20", " ")

    @Bean
    open fun androidDriver(): AndroidDriver<MobileElement> {
        val capabilities = DesiredCapabilities()
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "test")
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2)
        capabilities.setCapability(MobileCapabilityType.APP, getAppPath)
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.wdiodemoapp")
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.wdiodemoapp.MainActivity")
        capabilities.setCapability("unicodeKeyboard", true)
        capabilities.setCapability("resetKeyboard", true)
        appDriver = AndroidDriver(URL(url), capabilities)
        appDriver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS)
        return appDriver
    }

    @PreDestroy
    open fun quit() {
        appDriver.quit()
    }

    companion object {
        const val IMPLICITLY_WAIT = 10L
    }
}