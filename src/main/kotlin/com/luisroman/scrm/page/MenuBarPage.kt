package com.luisroman.scrm.page

import io.appium.java_client.MobileElement
import org.springframework.stereotype.Component

@Component
open class MenuBarPage: BasePage() {

    fun clickWebViewButton() {
        getWebViewButton().click()
    }

    fun clickLoginButton() {
        getLoginButton().click()
    }

    fun clickFormsButton() {
        getFormsButton().click()
    }

    private fun getWebViewButton(): MobileElement {
        return driver.findElementByXPath(WEBVIEW_BUTTON)
    }

    private fun getLoginButton(): MobileElement {
        return driver.findElementByXPath(LOGIN_BUTTON)
    }

    private fun getFormsButton(): MobileElement {
        return driver.findElementByXPath(FORMS_BUTTON)
    }

    companion object {
        private const val WEBVIEW_BUTTON = "//android.view.ViewGroup[@content-desc='WebView']/android.widget.TextView"
        private const val LOGIN_BUTTON = "//android.view.ViewGroup[@content-desc='Login']/android.widget.TextView"
        private const val FORMS_BUTTON = "//android.view.ViewGroup[@content-desc='Forms']/android.widget.TextView"
    }
}