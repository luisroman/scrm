package com.luisroman.scrm.page.webview

import com.luisroman.scrm.page.BasePage
import org.springframework.stereotype.Component

@Component
open class GettingStartedWebPage: BasePage() {

    fun checkIsGettingStartedCurrentUrl(): Boolean {
        return elementUtils.getCurrentUrl().contains(GETTING_STARTED_URL)
    }

    companion object {
        private const val GETTING_STARTED_URL = "/docs/gettingstarted.html"
    }

}