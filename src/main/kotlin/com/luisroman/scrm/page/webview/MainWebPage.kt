package com.luisroman.scrm.page.webview

import com.luisroman.scrm.page.BasePage
import org.openqa.selenium.WebElement
import org.springframework.stereotype.Component

@Component
open class MainWebPage: BasePage() {

    fun clickGetStartedButton() {
        elementUtils.waitForClickable(getGetStartedButton())
        getGetStartedButton().click()
    }

    private fun getGetStartedButton(): WebElement {
        return driver.findElementByXPath(GET_STARTED_BUTTON)
    }

    companion object {
        private const val GET_STARTED_BUTTON = "//a[@class='button' and @href='/docs/gettingstarted.html']"
    }

}