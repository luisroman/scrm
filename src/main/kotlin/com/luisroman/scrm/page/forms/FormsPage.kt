package com.luisroman.scrm.page.forms

import com.luisroman.scrm.page.BasePage
import io.appium.java_client.MobileElement
import org.springframework.stereotype.Component

@Component
open class FormsPage: BasePage() {

    fun clickSwitchButton() {
        elementUtils.scrollToClass(SWITCH_BUTTON)
        getSwitchButton().click()
    }

    fun isSwitchChecked(): Boolean {
        elementUtils.scrollToClass(SWITCH_BUTTON)
        return getSwitchButton().getAttribute("checked")!!.toBoolean()
    }

    fun clickAppiumIsAwesomeOption() {
        getAppiumIsAwesomeOption().click()
    }

    fun clickDropDownList() {
        elementUtils.scrollToClass(DROPDOWN_LIST)
        getDropDownList().click()
    }

    fun isAppiumIsAwesomeSelected(): Boolean {
        return getDropDownValue().text == APPIUM_IS_AWESOME_TEXT
    }

    fun isActiveButtonDisplayed(): Boolean {
        elementUtils.scrollToText(ACTIVE_BUTTON_TEXT)
        return getActiveButton().isDisplayed
    }

    fun clickActiveButton() {
        elementUtils.scrollToText(ACTIVE_BUTTON_TEXT)
        getActiveButton().click()
    }

    fun clickCancelPopupButton() {
        getCancelPopupButton().click()
    }

    fun isFormActivePopupDisplayed(): Boolean {
        return getFormActivePopup().isDisplayed
    }

    private fun getSwitchButton(): MobileElement {
        return driver.findElementByClassName(SWITCH_BUTTON)
    }

    private fun getAppiumIsAwesomeOption(): MobileElement {
        return driver.findElementByXPath(APPIUM_IS_AWESOME_OPTION)
    }

    private fun getDropDownList(): MobileElement {
        return driver.findElementByClassName(DROPDOWN_LIST)
    }

    private fun getDropDownValue(): MobileElement {
        return driver.findElementByXPath(DROPDOWN_VALUE)
    }

    private fun getActiveButton(): MobileElement {
        return driver.findElementByXPath(ACTIVE_BUTTON)
    }

    private fun getCancelPopupButton(): MobileElement {
        return driver.findElementById(CANCEL_POPUP_BUTTON)
    }

    private fun getFormActivePopup(): MobileElement {
        return driver.findElementByXPath(FORM_ACTIVE_POPUP)
    }

    companion object {
        private const val SWITCH_BUTTON = "android.widget.Switch"
        private const val APPIUM_IS_AWESOME_TEXT = "Appium is awesome"
        private const val APPIUM_IS_AWESOME_OPTION = "//android.widget.CheckedTextView[@text='$APPIUM_IS_AWESOME_TEXT']"
        private const val DROPDOWN_LIST = "android.widget.Spinner"
        private const val DROPDOWN_VALUE = "//android.view.ViewGroup[@content-desc='select-Dropdown']/android.widget.TextView[2]"
        private const val ACTIVE_BUTTON_TEXT = "Active"
        private const val FORM_ACTIVE_POPUP = "//*[@resource-id='android:id/alertTitle']"
        private const val ACTIVE_BUTTON = "//android.view.ViewGroup/android.widget.TextView[@text='$ACTIVE_BUTTON_TEXT']"
        private const val CANCEL_POPUP_BUTTON = "android:id/button2"
    }

}