package com.luisroman.scrm.page

import com.luisroman.scrm.utils.ElementUtils
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class BasePage {

    @Autowired
    protected lateinit var driver: AndroidDriver<MobileElement>

    @Autowired
    protected lateinit var elementUtils: ElementUtils

}