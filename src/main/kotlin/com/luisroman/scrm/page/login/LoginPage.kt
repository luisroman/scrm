package com.luisroman.scrm.page.login

import com.luisroman.scrm.page.BasePage
import io.appium.java_client.MobileElement
import org.springframework.stereotype.Component

@Component
open class LoginPage: BasePage() {

    fun fillEmail(email: String) {
        getInputEmail().sendKeys(email)
    }

    fun fillPassword(password: String) {
        getInputPassword().sendKeys(password)
    }

    fun clickLoginButton() {
        getLoginButton().click()
    }

    fun login(email: String, password: String) {
        fillEmail(email)
        fillPassword(password)
        clickLoginButton()
    }

    fun isSuccessLoginPopupDisplayed(): Boolean {
        return getSuccessLoginPopupTitle().isDisplayed
    }

    fun clickSuccessLoginPopupConfirmButton() {
        getSuccessLoginConfirmButton().click()
    }

    fun isLoginButtonDisplayed(): Boolean {
        return getLoginButton().isDisplayed
    }

    private fun getInputEmail(): MobileElement {
        return driver.findElementByXPath(INPUT_EMAIL)
    }

    private fun getInputPassword(): MobileElement {
        return driver.findElementByXPath(INPUT_PASSWORD)
    }

    private fun getLoginButton(): MobileElement {
        return driver.findElementByXPath(LOGIN_BUTTON)
    }

    private fun getSuccessLoginPopupTitle(): MobileElement {
        return driver.findElementByXPath(SUCCESS_LOGIN_POPUP_TITLE)
    }

    private fun getSuccessLoginConfirmButton(): MobileElement {
        return driver.findElementById(SUCCESS_LOGIN_OK_BUTTON)
    }

    companion object {
        private const val INPUT_EMAIL = "//android.widget.EditText[@content-desc='input-email']"
        private const val INPUT_PASSWORD = "//android.widget.EditText[@content-desc='input-password']"
        private const val LOGIN_BUTTON = "//android.view.ViewGroup/android.widget.TextView[@text='LOGIN']"
        private const val SUCCESS_LOGIN_POPUP_TITLE = "//*[@resource-id='android:id/alertTitle' and @text='Success']"
        private const val SUCCESS_LOGIN_OK_BUTTON = "android:id/button1"
    }
}