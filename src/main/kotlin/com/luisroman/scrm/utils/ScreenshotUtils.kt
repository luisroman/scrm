package com.luisroman.scrm.utils

import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import io.qameta.allure.Attachment
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class ScreenshotUtils {

    @Autowired
    private lateinit var driver: AndroidDriver<MobileElement>

    @Attachment("Test Screenshot")
    fun takeScreenshot(): ByteArray {
        return (driver as TakesScreenshot).getScreenshotAs(OutputType.BYTES)
    }
}