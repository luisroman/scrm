package com.luisroman.scrm.utils

import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class ElementUtils {

    @Autowired
    private lateinit var driver: AndroidDriver<MobileElement>

    fun waitForClickable(element: WebElement) {
        val wait = WebDriverWait(driver, IMPLICITLY_WAIT)
        wait.until(ExpectedConditions.elementToBeClickable(element))
    }

    fun getCurrentUrl(): String {
        return driver.currentUrl
    }

    fun scrollToText(text: String) {
        driver.findElementByAndroidUIAutomator(
            "new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"$text\"));"
        )
    }

    fun scrollToClass(className: String) {
        driver.findElementByAndroidUIAutomator(
            "new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().className(\"$className\"));"
        )
    }

    companion object {
        private const val IMPLICITLY_WAIT = 20L
    }

}