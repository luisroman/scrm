package com.luisroman.scrm.utils

import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class ContextUtils {

    @Autowired
    private lateinit var driver: AndroidDriver<MobileElement>

    fun changeToWebView() {
        val contextNames = driver.contextHandles
        driver.context(contextNames.toTypedArray()[1])
    }

    fun changeToNative() {
        driver.context("NATIVE_APP")
    }

}