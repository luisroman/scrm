package com.luisroman.scrm

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class SpringConfiguration {

    companion object {
        fun main(args: Array<String>) {
            SpringApplication.run(SpringConfiguration::class.java, *args)
        }
    }
}